import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

import { Article } from './interfaces/article.interface';
import { CreateArticleDto } from './dto/create-article.dto';
import { RedisService } from 'nestjs-redis/dist/redis.service';

@Injectable()
export class ArticleService {
    private redisClient;
    constructor(
        @InjectModel('Article') private readonly articleModel: Model<Article>,
        private readonly redisService: RedisService
    ) { 
        this.redisClient = this.redisService.getClient();

    }

    // ┌─┐┬─┐┌─┐┌─┐┌┬┐┌─┐  ┌─┐┬─┐┌┬┐┬┌─┐┬  ┌─┐
    // │  ├┬┘├┤ ├─┤ │ ├┤   ├─┤├┬┘ │ ││  │  ├┤ 
    // └─┘┴└─└─┘┴ ┴ ┴ └─┘  ┴ ┴┴└─ ┴ ┴└─┘┴─┘└─┘

    async createArticle(createArticleDto: CreateArticleDto): Promise<Article> {
        const article = new this.articleModel(createArticleDto);
        await article.save();
        return article;
    }

    // ┌─┐┌─┐┌┬┐  ┌─┐┬  ┬    ┌─┐┬─┐┌┬┐┬┌─┐┬  ┌─┐┌─┐
    // │ ┬├┤  │   ├─┤│  │    ├─┤├┬┘ │ ││  │  ├┤ └─┐
    // └─┘└─┘ ┴   ┴ ┴┴─┘┴─┘  ┴ ┴┴└─ ┴ ┴└─┘┴─┘└─┘└─┘
    async getAllArticles(): Promise<any> {
        const article = await this.redisClient.get('allArticle');
        if (!article) {
          console.log('return from DB');
          const result = await this.articleModel.find({});
          await this.redisClient.set('allArticle', "result", 'EX', 20);
          return result;
        }

        console.log('return from redis');
        return article;
      }
      
    

    // ┌─┐┌─┐┌┬┐  ┌─┐┌┐┌┌─┐  ┌─┐┬─┐┌┬┐┬┌─┐┬  ┌─┐
    // │ ┬├┤  │   │ ││││├┤   ├─┤├┬┘ │ ││  │  ├┤ 
    // └─┘└─┘ ┴   └─┘┘└┘└─┘  ┴ ┴┴└─ ┴ ┴└─┘┴─┘└─┘
    async getOneArticle(id: string): Promise<Article> {
        const article = await this.redisClient.get('oneArticle');
        if (!article) {
          console.log('return from DB');
          const result =  await this.articleModel.findById(id)
          await this.redisClient.set('oneArticle', "result", 'EX', 20);
          return result;
        }

        console.log('return from redis');
        return article;
      }

    // ┬ ┬┌─┐┌┬┐┌─┐┌┬┐┌─┐  ┌─┐┬─┐┌┬┐┬┌─┐┬  ┌─┐    ┌─┐┬  ┬    ┌─┐┌─┐┬─┐┌─┐┌┬┐┌─┐  
    // │ │├─┘ ││├─┤ │ ├┤   ├─┤├┬┘ │ ││  │  ├┤     ├─┤│  │    ├─┘├─┤├┬┘├─┤│││└─┐  
    // └─┘┴  ─┴┘┴ ┴ ┴ └─┘  ┴ ┴┴└─ ┴ ┴└─┘┴─┘└─┘    ┴ ┴┴─┘┴─┘  ┴  ┴ ┴┴└─┴ ┴┴ ┴└─┘  \
    async updateArticlePut(id: string, createArticleDto: CreateArticleDto): Promise<Article> {
        return await this.articleModel.updateOne({_id: id}, createArticleDto);
    }

    // ┌┬┐┌─┐┬  ┌─┐┌┬┐┌─┐  ┌─┐┌┐┌┌─┐  ┌─┐┬─┐┌┬┐┬┌─┐┬  ┌─┐
    //  ││├┤ │  ├┤  │ ├┤   │ ││││├┤   ├─┤├┬┘ │ ││  │  ├┤ 
    // ─┴┘└─┘┴─┘└─┘ ┴ └─┘  └─┘┘└┘└─┘  ┴ ┴┴└─ ┴ ┴└─┘┴─┘└─┘
    async deleteArticle(id: string): Promise<Article> {
        return await this.articleModel.findByIdAndDelete(id);
    }
}
