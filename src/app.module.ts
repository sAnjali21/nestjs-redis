import { Module } from '@nestjs/common';
import { AppController } from './app.controller';


import { AppService } from './app.service';
import { MongooseModule } from '@nestjs/mongoose';
import { UserModule } from './user/user.module';
import { AuthModule } from './auth/auth.module';
import { ArticleModule } from './article/article.module';
import { RedisModule } from 'nestjs-redis/dist/redis.module';
@Module({
  imports: [MongooseModule.forRoot(process.env.MONGO_URI),
    RedisModule.register({
      host: 'localhost',
      port: 6379,
    }),
    UserModule, AuthModule, ArticleModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
